// Dependencies
// import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import axios from '../../axios/axios-orders';
import Burger from '../../components/Burger';
import ControlPanel from "../../components/ControlPanel";
import OrderSummary from "../../components/OrderSummary";
import Modal from "../../components/UI/Modal";
import Spinner from "../../components/UI/Spinner";

// Constants
import { BURGER_INGREDIENTS__OPTIONAL } from '../../components/Burger/BurgerIngredient/BurgerIngredient.constants';

// Assets

// Types and Interfaces
enum BURGER_DELIVERY_METHODS {
  'pickUp',
  'deliveryNormal',
  'deliveryPriorty',
}

type SelectedIngredientsCount = {
  [ingredient in BURGER_INGREDIENTS__OPTIONAL]: number;
};

type SelectedIngredientsBool = {
  [ingredient in BURGER_INGREDIENTS__OPTIONAL]: boolean;
} | {};

interface IStateBurgerBuilder {
  isCheckingOut: boolean;
  isLoading: boolean;
  ingredients: SelectedIngredientsCount;
  purchasable: boolean;
  totalPrice: number;
}

interface ICustomerData {
  address: {
    country: string,
    postalCode: number,
    street: string,
    town: string,
  };
  email: string;
  name: string;
}

interface IBurgerOrder {
  customer: ICustomerData;
  deliveryMethod: BURGER_DELIVERY_METHODS;
  selectedIngredients: SelectedIngredientsCount;
  totalPrice: number;
}

// Component
class BurgerBuilder extends React.Component {
  public state: IStateBurgerBuilder = {
    ingredients: {
      [BURGER_INGREDIENTS__OPTIONAL.bacon]: 0,
      [BURGER_INGREDIENTS__OPTIONAL.salad]: 0,
      [BURGER_INGREDIENTS__OPTIONAL.cheese]: 0,
      [BURGER_INGREDIENTS__OPTIONAL.meat]: 0,
    },
    isCheckingOut: false,
    isLoading: false,
    purchasable: false,
    totalPrice: 2,
  };

  public BURGER_INGREDIENTS_PRICES: SelectedIngredientsCount = {
    [BURGER_INGREDIENTS__OPTIONAL.bacon]: 0.7,
    [BURGER_INGREDIENTS__OPTIONAL.salad]: 0.2,
    [BURGER_INGREDIENTS__OPTIONAL.cheese]: 0.5,
    [BURGER_INGREDIENTS__OPTIONAL.meat]: 1.5,
  };

  public render(): React.ReactNode {
    const enabledIngredients: SelectedIngredientsBool = {};
    const selectedIngredients: SelectedIngredientsCount = { ...this.state.ingredients };
    for (const key in selectedIngredients) {
      if (selectedIngredients.hasOwnProperty(key)) {
        enabledIngredients[key] = selectedIngredients[key] > 0;
      }
    }

    const getOrderSummary = (): React.ReactNode => {
      if (this.state.isLoading) {
        return <Spinner />;
      } else {
        return <OrderSummary ingredients={this.state.ingredients}
                             continueCheckoutOrderHandler={this.proceedCheckoutOrder}
                             cancelCheckoutOrderHandler={this.cancelCheckOutOrder}
                             totalPrice={this.state.totalPrice} />;
      }
    };

    return (
      <>
        <Modal isVisible={this.state.isCheckingOut}
               modalCloseHandler={this.cancelCheckOutOrder}>
          {getOrderSummary()}
        </Modal>
        <Burger selectedIngredients={this.state.ingredients} />
        <ControlPanel listOfIngredients={this.state.ingredients}
                      lessIngredientHandler={this.addIngredient}
                      moreIngredientHandler={this.removeIngredient}
                      totalPrice={this.state.totalPrice}
                      isPurchasable={this.state.purchasable}
                      enabledIngredients={enabledIngredients}
                      onClickOrderHandler={this.initCheckOutOrder}
        />
      </>
    );
  }

  protected proceedCheckoutOrder = (): void => {
    const nextState: Partial<IStateBurgerBuilder> = { isLoading: true };
    this.setState(nextState);

    const order: IBurgerOrder = {
      customer: {
        address: {
          country: 'New Zealand',
          postalCode: 3114,
          street: '20 Omokoroa Rd.',
          town: 'Omokoroa',
        },
        email: 'nadira@gmail.com',
        name: 'Nadira Chua',
      },
      deliveryMethod: BURGER_DELIVERY_METHODS.deliveryNormal,
      selectedIngredients: this.state.ingredients,
      totalPrice: this.state.totalPrice,
    };
    console.log('Continue ordering: ', order); // tslint:disable-line:no-console

    axios.post('/burger-orders.json', order)
      .then(response => {
        const nextStateOnResponse: Partial<IStateBurgerBuilder> = { isLoading: false, isCheckingOut: false };
        this.setState(nextStateOnResponse);
        console.log('response: ', response); // tslint:disable-line:no-console
      })
      .catch(err => {
        const nextStateOnError: Partial<IStateBurgerBuilder> = { isLoading: false, isCheckingOut: false };
        this.setState(nextStateOnError);
        console.error(err); // tslint:disable-line:no-console
      });
  };

  protected cancelCheckOutOrder = (): void => {
    const nextState: Partial<IStateBurgerBuilder> = { isCheckingOut: false };
    this.setState(nextState);
  };

  protected initCheckOutOrder = (): void => {
    const nextState: Partial<IStateBurgerBuilder> = { isCheckingOut: true };
    this.setState(nextState);
  };

  protected updatePurchasableState = (nextStateIngredients: SelectedIngredientsCount): void => {
    const selectedIngredientsCopy: SelectedIngredientsCount = { ...nextStateIngredients };
    const selectedIngredientsArr = Object.entries(selectedIngredientsCopy);

    const sum: number = selectedIngredientsArr.reduce((result: number, [ingredient, count]: [string, number]) => {
      return result + count;
    }, 0);

    const nextState: Partial<IStateBurgerBuilder> = { purchasable: sum > 0 };
    this.setState(nextState);
  };

  protected addIngredient = (ingredient: BURGER_INGREDIENTS__OPTIONAL): void => {
    const ingredientCount = this.state.ingredients[ingredient] as number + 1;
    const nextStateIngredients: SelectedIngredientsCount = {
      ...this.state.ingredients,
      ...{ [ingredient]: ingredientCount },

    };
    const nextStateTotalPrice = this.state.totalPrice + this.BURGER_INGREDIENTS_PRICES[ingredient];

    const nextState: Partial<IStateBurgerBuilder> = {
      ingredients: nextStateIngredients,
      totalPrice: nextStateTotalPrice,
    };
    this.updatePurchasableState(nextStateIngredients);
    this.setState(nextState);

  };

  protected removeIngredient = (ingredient: BURGER_INGREDIENTS__OPTIONAL): void => {
    if (this.state.ingredients[ingredient] as number > 0) {
      const ingredientCount = this.state.ingredients[ingredient] as number - 1;
      const nextStateIngredients: SelectedIngredientsCount = {
        ...this.state.ingredients,
        ...{ [ingredient]: ingredientCount },
      };
      const nextStateTotalPrice = this.state.totalPrice - this.BURGER_INGREDIENTS_PRICES[ingredient];

      const nextState: Partial<IStateBurgerBuilder> = {
        ingredients: nextStateIngredients,
        totalPrice: nextStateTotalPrice,
      };
      this.updatePurchasableState(nextStateIngredients);
      this.setState(nextState);
    }
  };
}

// Exports
export {
  SelectedIngredientsBool,
  SelectedIngredientsCount,
};
export default BurgerBuilder;
