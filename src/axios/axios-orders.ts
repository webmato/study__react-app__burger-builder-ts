import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-builder-f5780.firebaseio.com/',
});

export default instance;
