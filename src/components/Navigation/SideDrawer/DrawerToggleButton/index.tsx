// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './DrawerToggleButton.css';

// Types and Interfaces
interface IDrawerToggleButtonProps {
  onClickHandler: () => void;
}

// Component
const DrawerToggleButton: React.FunctionComponent<IDrawerToggleButtonProps> = (props) => {
  return (
    <div className={classes.DrawerToggleButton} onClick={props.onClickHandler}>
      <div />
      <div />
      <div />
    </div>
  );
};

DrawerToggleButton.propTypes = {
  onClickHandler: PropTypes.func.isRequired,
};

// Exports
export default DrawerToggleButton;
