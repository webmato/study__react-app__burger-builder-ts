interface ICssClasses {
  DrawerToggleButton: string;
}

declare const styles: ICssClasses;

export default styles;
