// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import Logo from "../../Logo";
import Backdrop from "../../UI/Backdrop";
import Navigation from "../Navigation/";

// Assets
import classes from './SideDrawer.css';

// Types and Interfaces
interface IPropsSideDrawer {
  sideDrawerIsVisible: boolean;
  closeSideDrawer: () => void;
}

// Component
const SideDrawer: React.FunctionComponent<IPropsSideDrawer> = (props) => {
  const sideDrawerClasses = [classes.SideDrawer, props.sideDrawerIsVisible ? classes.Open : classes.Close].join(' ');

  return (
    <>
      <Backdrop backdropOnClickHandler={props.closeSideDrawer} isVisible={props.sideDrawerIsVisible} />
      <div className={sideDrawerClasses}>
        <div className={classes.Logo}>
          <Logo />
        </div>
        <Navigation />
      </div>
    </>
  );
};

SideDrawer.propTypes = {
  closeSideDrawer: PropTypes.func.isRequired,
  sideDrawerIsVisible: PropTypes.bool.isRequired,
};

// Exports
export default SideDrawer;
