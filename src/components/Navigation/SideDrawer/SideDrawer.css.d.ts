interface ICssClasses {
  Close: string;
  Logo: string;
  Open: string;
  SideDrawer: string;
}

declare const styles: ICssClasses;

export default styles;
