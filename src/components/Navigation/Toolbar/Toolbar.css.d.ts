interface ICssClasses {
  DesktopOnly: string;
  Logo: string;
  Toolbar: string;
}

declare const styles: ICssClasses;

export default styles;
