// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import Logo from "../../Logo";
import Navigation from "../Navigation";
import DrawerToggleButton from "../SideDrawer/DrawerToggleButton";

// Assets
import classes from './Toolbar.css';

// Types and Interfaces
interface IToolbarProps {
  sideDrawerToggleOnClick: () => void;
}

// Component
const Toolbar: React.FunctionComponent<IToolbarProps> = (props) => {
  return (
    <header className={classes.Toolbar}>
      <DrawerToggleButton onClickHandler={props.sideDrawerToggleOnClick} />
      <div className={classes.Logo}>
        <Logo />
      </div>
      <Navigation classes={classes.DesktopOnly} />
    </header>
  );
};

Toolbar.propTypes = {
  sideDrawerToggleOnClick: PropTypes.func.isRequired,
};

// Exports
export default Toolbar;
