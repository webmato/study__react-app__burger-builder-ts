// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import NavigationItems from "./NavigationItems";

// Assets
import classes from './Navigation.css';

// Types and Interfaces
interface IPropsNavigation {
  classes?: string;
}

// Component
const Navigation: React.FunctionComponent<IPropsNavigation> = (props) => {
  const navigationClasses: string = [classes.Navigation, props.classes ? props.classes : null].join(' ');

  return (
    <nav className={navigationClasses}>
      <NavigationItems />
    </nav>
  );
};

Navigation.propTypes = {
  classes: PropTypes.string,
};

// Exports
export default Navigation;
