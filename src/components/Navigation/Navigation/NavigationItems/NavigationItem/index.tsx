// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './NavigationItem.css';

// Types and Interfaces
interface INavigationItemProps {
  children: React.ReactNode;
  isActive?: boolean;
  link: string;
}

// Component
const NavigationItem: React.FunctionComponent<INavigationItemProps> = (props) => {
  return (
    <li className={classes.NavigationItem}>
      <a href={props.link}
         className={props.isActive ? classes.active : undefined}>
        {props.children}
      </a>
    </li>
  );
};

NavigationItem.propTypes = {
  children: PropTypes.node.isRequired,
  isActive: PropTypes.bool,
  link: PropTypes.string.isRequired,
};

// Exports
export default NavigationItem;
