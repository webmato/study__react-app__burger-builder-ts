interface ICssClasses {
  NavigationItem: string;
  active: string;
}

declare const styles: ICssClasses;

export default styles;
