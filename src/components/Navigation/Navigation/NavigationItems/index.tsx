// Dependencies
import * as React from 'react';

// Modules
import NavigationItem from "./NavigationItem";

// Assets
import classes from './NavigationItems.css';

// Types and Interfaces

// Component
const NavigationItems: React.FunctionComponent = () => {
  return (
    <ul className={classes.NavigationItems}>
        <NavigationItem link="/" isActive={true}>Burger Builder</NavigationItem>
        <NavigationItem link="/">Checkout</NavigationItem>
      </ul>
  );
};

// Exports
export default NavigationItems;
