// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import { SelectedIngredientsCount } from "../../containers/BurgerBuilder";
import Button from "../UI/Button";

// Constants
import { BUTTON_TYPES } from "../UI/Button/Button.constants";

// Assets
import classes from './OrderSummary.css';

// Types and Interfaces
type Ingredient = [string, number];

interface IOrderSummaryProps {
  cancelCheckoutOrderHandler: () => void;
  continueCheckoutOrderHandler: () => void;
  ingredients: SelectedIngredientsCount;
  totalPrice: number;
}

/*
 *  This component could be a pure functional component.
  * Its turned to class component because of educational/debugging reasons.
  * See the componentWillUpdate() method. It is updating even the component is not visible.
  *
  * We will solve this by shouldComponentUpdate() in parent Modal component.
 */

// Component
class OrderSummary extends React.Component<IOrderSummaryProps> {
  public static propTypes = {
    cancelCheckoutOrderHandler: PropTypes.func.isRequired,
    continueCheckoutOrderHandler: PropTypes.func.isRequired,
    // ingredients: PropTypes.???.isRequired, // TODO: add correct prop type
    totalPrice: PropTypes.number.isRequired,
  };

  public componentWillUpdate(): void {
    console.log('---( [OrderSummary]componentWillUpdate )-------------------------------------------------'); // tslint:disable-line:no-console
  }

  public render(): React.ReactNode {
    const selectedIngredientsArr: Ingredient[] = Object.entries(this.props.ingredients);

    const getListItem = (ingredientName: string, count: number): React.ReactNode => {
      return (
        <li className={classes.ListItem} key={ingredientName}>
          <span>{ingredientName}: {count}</span>
        </li>
      );
    };

    const getListItems = (ingredients: Ingredient[]): React.ReactNode => {
      return ingredients.map(([ingredientName, count]: [string, number]) => getListItem(ingredientName, count));
    };

    return (
      <>
        <h3>Your Order</h3>
        <p>Delicious burger with following ingredients:</p>
        <ul>
          {getListItems(selectedIngredientsArr)}
        </ul>
        <p><strong>Total Price: ${this.props.totalPrice.toFixed(2)}</strong></p>
        <p>Continue to checkout?</p>
        <Button buttonType={BUTTON_TYPES.danger} onClickHandler={this.props.cancelCheckoutOrderHandler}>CANCEL</Button>
        <Button buttonType={BUTTON_TYPES.success}
                onClickHandler={this.props.continueCheckoutOrderHandler}>CONTINUE</Button>
      </>
    );
  }
}


// Exports
export default OrderSummary;
