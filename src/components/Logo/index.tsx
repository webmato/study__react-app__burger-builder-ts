// Dependencies
import * as React from 'react';

// Modules

// Assets
import logoImage from '../../assets/images/burger-logo.png';
import classes from './Logo.css';

// Component
const Logo: React.FunctionComponent = () => {
  return (
    <div className={classes.Logo}>
      <img src={logoImage} alt="Burger Builder" />
    </div>
  );
};

// Exports
export default Logo;
