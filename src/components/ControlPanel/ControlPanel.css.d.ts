interface ICssClasses {
  BuildControls: string;
  OrderButton: string;
}

declare const styles: ICssClasses;

export default styles;
