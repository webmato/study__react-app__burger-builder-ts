interface ICssClasses {
  ControlUnit: string;
  Label: string;
  Less: string;
  More: string;
}

declare const styles: ICssClasses;

export default styles;
