// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './ControlUnit.css';

// Types and Interfaces
interface IControlUnitProps {
  count: number;
  label: string;
  ingredientType: string;
  isEnabled: boolean;
  moreOnClickHandler: (ingredientType: string) => void;
  lessOnClickHandler: (ingredientType: string) => void;
}

// Component
const ControlUnit: React.FunctionComponent<IControlUnitProps> = (props: IControlUnitProps) => {
  function _moreOnClickHandler() {
    props.moreOnClickHandler(props.ingredientType);
  }

  function _lessOnClickHandler() {
    props.lessOnClickHandler(props.ingredientType);
  }

  return (
    <div className={classes.ControlUnit}>
      <div className={classes.Label}>{props.label}</div>
      <button className={classes.More} onClick={_moreOnClickHandler}>+</button>
      <button className={classes.Less} onClick={_lessOnClickHandler} disabled={!props.isEnabled}>-</button>
    </div>
  );
};

ControlUnit.propTypes = {
  count: PropTypes.number.isRequired,
  ingredientType: PropTypes.string.isRequired,
  isEnabled: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  lessOnClickHandler: PropTypes.func.isRequired,
  moreOnClickHandler: PropTypes.func.isRequired,
};

// Exports
export default ControlUnit;
