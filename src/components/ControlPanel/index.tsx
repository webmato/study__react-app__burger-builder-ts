// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import {
  SelectedIngredientsBool,
  SelectedIngredientsCount,
} from "../../containers/BurgerBuilder";
import ControlUnit from "./ControlUnit";

// Constants
import { BURGER_INGREDIENTS__OPTIONAL } from "../Burger/BurgerIngredient/BurgerIngredient.constants";

// Assets
import classes from './ControlPanel.css';

// Types and Interfaces
interface IControlPanelProps {
  isPurchasable: boolean;
  listOfIngredients: SelectedIngredientsCount;
  lessIngredientHandler: (ingredient: string) => void;
  moreIngredientHandler: (ingredient: string) => void;
  totalPrice: number;
  enabledIngredients: SelectedIngredientsBool;
  onClickOrderHandler: () => void;
}


// Component
const ControlPanel: React.FunctionComponent<IControlPanelProps> = (props) => {
  const getControlUnitsNode = (ingredient: BURGER_INGREDIENTS__OPTIONAL,
                               isDisabled: boolean,
                               count: number,
                               reactKey: string | number): React.ReactNode => {
    return <ControlUnit key={reactKey}
                        count={count}
                        label={ingredient}
                        ingredientType={ingredient}
                        isEnabled={isDisabled}
                        moreOnClickHandler={props.lessIngredientHandler}
                        lessOnClickHandler={props.moreIngredientHandler} />;
  };

  const getControlUnitsNodes = (): React.ReactNode => {
    const controlUnitsNodes = [];
    for (const ingredient in props.listOfIngredients) {
      if (props.listOfIngredients.hasOwnProperty(ingredient)) {
        controlUnitsNodes.push(getControlUnitsNode(
          ingredient as BURGER_INGREDIENTS__OPTIONAL,
          props.enabledIngredients[ingredient],
          props.listOfIngredients[ingredient],
          ingredient));
      }
    }
    return controlUnitsNodes;
  };

  return (
    <div>
      ControlPanel
      <div className={classes.BuildControls}>
        <div>Total price: <strong>${props.totalPrice.toFixed(2)}</strong></div>
        {getControlUnitsNodes()}
        <button className={classes.OrderButton}
                onClick={props.onClickOrderHandler}
                disabled={!props.isPurchasable}>
          ORDER NOW
        </button>
      </div>
    </div>
  );
};

ControlPanel.propTypes = {
  // enabledIngredients: PropTypes.???.isRequired, // TODO: add correct prop type
  isPurchasable: PropTypes.bool.isRequired,
  lessIngredientHandler: PropTypes.func.isRequired,
  // listOfIngredients: PropTypes.???.isRequired, // TODO: add correct prop type
  moreIngredientHandler: PropTypes.func.isRequired,
  onClickOrderHandler: PropTypes.func.isRequired,
  totalPrice: PropTypes.number.isRequired,
};

// Exports
export default ControlPanel;
