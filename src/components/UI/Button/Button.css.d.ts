interface ICssClasses {
  Button: string;
  Danger: string;
  Success: string;
}

declare const styles: ICssClasses;

export default styles;
