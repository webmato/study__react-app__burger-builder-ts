// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './Button.css';

// Types and Interfaces
interface IButtonProps {
  children: React.ReactNode;
  buttonType?: string;
  onClickHandler: (evt: React.MouseEvent<HTMLButtonElement>) => void;
}

// Component
const Button: React.FunctionComponent<IButtonProps> = (props) => {
  const buttonClasses = [classes.Button, props.buttonType ? classes[props.buttonType] : ''].join(' ');

  return (
    <button className={buttonClasses}
            onClick={props.onClickHandler}>
      {props.children}
    </button>
  );
};

Button.propTypes = {
  buttonType: PropTypes.string,
  children: PropTypes.node.isRequired,
  onClickHandler: PropTypes.func.isRequired,
};

// Exports
export default Button;
