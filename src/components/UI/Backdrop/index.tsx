// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Assets
import classes from './Backdrop.css';

// Types and Interfaces
interface IBackdropProps {
  backdropOnClickHandler: () => void;
  isVisible: boolean;
}

// Component
const Backdrop: React.FunctionComponent<IBackdropProps> = (props) => {
  return (
    props.isVisible
      ? <div className={classes.Backdrop} onClick={props.backdropOnClickHandler} />
      : null
  );
};

Backdrop.propTypes = {
  backdropOnClickHandler: PropTypes.func.isRequired,
  isVisible: PropTypes.bool.isRequired,
};

// Exports
export default Backdrop;
