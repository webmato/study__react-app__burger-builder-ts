interface ICssClasses {
  Backdrop: string;
}

declare const styles: ICssClasses;

export default styles;
