// Dependencies
import * as React from 'react';

// Modules

// Assets
import classes from './Spinner.css';

// Types and Interfaces

// Component
const Spinner: React.FunctionComponent = () => {
  return (
    <div className={classes.Spinner}>Loading...</div>
  );
};

// Exports
export default Spinner;
