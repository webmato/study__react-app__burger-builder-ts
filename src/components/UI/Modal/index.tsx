// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import Backdrop from "../Backdrop";

// Assets
import classes from './Modal.css';

// Types and Interfaces
interface IModalProps {
  children: React.ReactNode;
  isVisible: boolean;
  modalCloseHandler: () => void;
}

// Component
class Modal extends React.Component<IModalProps> {
  public static propTypes = {
    children: PropTypes.node.isRequired,
    isVisible: PropTypes.bool.isRequired,
    modalCloseHandler: PropTypes.func.isRequired,
  };

  public componentWillUpdate(): void {
    console.log('---( [Modal]componentWillUpdate )-------------------------------------------------'); // tslint:disable-line:no-console
  }

  public shouldComponentUpdate(nextProps: Readonly<IModalProps>): boolean {
    console.log('---( [Modal]shouldComponentUpdate )-------------------------------------------------'); // tslint:disable-line:no-console
    return nextProps.isVisible !== this.props.isVisible || nextProps.children !== this.props.children;
  }

  public render(): React.ReactNode {
    return (
      <>
        <Backdrop isVisible={this.props.isVisible} backdropOnClickHandler={this.props.modalCloseHandler} />
        <div className={classes.Modal}
             style={{
               opacity: this.props.isVisible ? 1 : 0,
               transform: this.props.isVisible ? 'translateY(0)' : 'translateY(-100vh)',
             }}>
          {this.props.children}
        </div>
      </>
    );
  }
}

// Exports
export default Modal;
