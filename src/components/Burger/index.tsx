// Dependencies
// import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import {
  SelectedIngredientsCount,
} from '../../containers/BurgerBuilder';

import BurgerIngredient from './BurgerIngredient';

// Constants
import {
  BURGER_INGREDIENT,
  BURGER_INGREDIENTS__MANDATORY,
  BURGER_INGREDIENTS__OPTIONAL,
} from "./BurgerIngredient/BurgerIngredient.constants";

// Assets
import classes from './Burger.css';

// Types and Interfaces
interface IBurgerProps {
  selectedIngredients: {
    [ingredient in BURGER_INGREDIENTS__OPTIONAL]: number;
  };
}

// Component
const Burger: React.FunctionComponent<IBurgerProps> = (props) => {
  const arrayOfSelectedIngredients = getArrayOfSelectedIngredients(props.selectedIngredients);

  const getBurgerIngredientNode = (ingredient: BURGER_INGREDIENT, elmKey?: string): React.ReactNode => {
    if (elmKey) {
      return <BurgerIngredient key={elmKey} type={ingredient} />;
    }
    return <BurgerIngredient type={ingredient} />;
  };

  const getSelectedBurgerIngredientNodes = (): React.ReactNode => {
    if (arrayOfSelectedIngredients.length > 0) {
      return arrayOfSelectedIngredients.map((ingredient: BURGER_INGREDIENTS__OPTIONAL, index: number) => {
        return getBurgerIngredientNode(ingredient, ingredient + index);
      });
    }
    return <em>Please start adding ingredients!</em>;
  };

  return (
    <div className={classes.Burger}>
      {getBurgerIngredientNode(BURGER_INGREDIENTS__MANDATORY.breadTop)}
      {getSelectedBurgerIngredientNodes()}
      {getBurgerIngredientNode(BURGER_INGREDIENTS__MANDATORY.breadBottom)}
    </div>
  );
};

function getArrayOfSelectedIngredients(ingredients: SelectedIngredientsCount): BURGER_INGREDIENTS__OPTIONAL[] {
  const ingredientsKeys: BURGER_INGREDIENTS__OPTIONAL[] = Object.keys(ingredients) as BURGER_INGREDIENTS__OPTIONAL[];

  return ingredientsKeys.reduce((accumulatorArray: BURGER_INGREDIENTS__OPTIONAL[], ingredientKey: BURGER_INGREDIENTS__OPTIONAL) => {
    const count = ingredients[ingredientKey];
    const nestedArrayOfIngredients: BURGER_INGREDIENTS__OPTIONAL[] = Array(count).fill(ingredientKey);
    return [...accumulatorArray, ...nestedArrayOfIngredients];
  }, []);
}

Burger.propTypes = {
//   selectedIngredients: PropTypes.???, // TODO: add correct prop type
};

// Exports
export default Burger;

