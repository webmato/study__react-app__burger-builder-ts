interface ICssClasses {
  BreadBottom: string;
  BreadTop: string;
  Seeds1: string;
  Seeds2: string;
  Meat: string;
  Cheese: string;
  Salad: string;
  Bacon: string;
}

declare const styles: ICssClasses;

export default styles;
