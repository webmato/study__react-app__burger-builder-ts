// Dependencies
// import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules

// Constants
import {
  BURGER_INGREDIENT,
  BURGER_INGREDIENTS__MANDATORY,
  BURGER_INGREDIENTS__OPTIONAL,
} from './BurgerIngredient.constants';

// Assets
import classes from './BurgerIngredient.css';

// Types and Interfaces
interface IPropsBurgerIngredient {
  type: BURGER_INGREDIENT;
}

// Component
const BurgerIngredient: React.FunctionComponent<IPropsBurgerIngredient> = (props) => {
  let ingredient: React.ReactChild | null;

  switch (props.type) {
    case (BURGER_INGREDIENTS__MANDATORY.breadBottom):
      ingredient = <div className={classes.BreadBottom} />;
      break;

    case (BURGER_INGREDIENTS__MANDATORY.breadTop):
      ingredient = <div className={classes.BreadTop}>
        <div className={classes.Seeds1} />
        <div className={classes.Seeds2} />
      </div>;
      break;

    case (BURGER_INGREDIENTS__OPTIONAL.meat):
      ingredient = <div className={classes.Meat} />;
      break;

    case (BURGER_INGREDIENTS__OPTIONAL.cheese):
      ingredient = <div className={classes.Cheese} />;
      break;

    case (BURGER_INGREDIENTS__OPTIONAL.bacon):
      ingredient = <div className={classes.Bacon} />;
      break;

    case (BURGER_INGREDIENTS__OPTIONAL.salad):
      ingredient = <div className={classes.Salad} />;
      break;

    default:
      ingredient = null;
  }

  return ingredient;
};

BurgerIngredient.propTypes = {
  // type: PropTypes.objectOf(BURGER_INGREDIENT), // TODO: add correct prop type
};

// Exports
export {
  IPropsBurgerIngredient,
};
export default BurgerIngredient;
