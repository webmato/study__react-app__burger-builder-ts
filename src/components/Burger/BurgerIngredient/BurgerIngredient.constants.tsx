export enum BURGER_INGREDIENTS__MANDATORY {
  breadBottom = 'bread-bottom',
  breadTop = 'bread-top',
}

export enum BURGER_INGREDIENTS__OPTIONAL {
  meat = 'meat',
  bacon = 'bacon',
  salad = 'salad',
  cheese = 'cheese',
}

export type BURGER_INGREDIENT = BURGER_INGREDIENTS__MANDATORY | BURGER_INGREDIENTS__OPTIONAL;
