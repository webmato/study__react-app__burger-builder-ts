// Dependencies
import * as PropTypes from 'prop-types';
import * as React from 'react';

// Modules
import SideDrawer from "../../components/Navigation/SideDrawer";
import Toolbar from "../../components/Navigation/Toolbar";

// Assets
import classes from './Layout.css';

// Types and Interfaces
interface ILayoutProps {
  children: React.ReactNode;
}

interface ILayoutState {
  sideDrawerIsVisible: boolean;
}

// Component
class Layout extends React.Component<ILayoutProps, ILayoutState> {
  public static propTypes = {
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
  };

  public state: ILayoutState = {
    sideDrawerIsVisible: false,
  };

  public render(): React.ReactNode {
    return (
      <>
        <Toolbar sideDrawerToggleOnClick={this.sideDrawerToggleOnClickHandler} />
        <SideDrawer closeSideDrawer={this.sideDrawerClosedHandler}
                    sideDrawerIsVisible={this.state.sideDrawerIsVisible} />
        <main className={classes.Content}>
          {this.props.children}
        </main>
      </>
    );
  }

  protected sideDrawerToggleOnClickHandler = (): void => {
    this.setState((prevState: ILayoutState): ILayoutState => {
      return { sideDrawerIsVisible: !prevState.sideDrawerIsVisible };
    });
  };

  protected sideDrawerClosedHandler = (): void => {
    const nextState: ILayoutState = { sideDrawerIsVisible: false };
    this.setState(nextState);
  };
}

// Exports
export default Layout;
