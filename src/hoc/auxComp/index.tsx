const AuxComp = (props: { children: JSX.Element }): JSX.Element => props.children;
export default AuxComp;
