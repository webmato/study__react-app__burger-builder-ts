// Dependencies
import * as React from 'react';

// Modules
import BurgerBuilder from "./containers/BurgerBuilder";
import Layout from './hoc/Layout';

// Assets

// Types and Interfaces

// Component
class App extends React.Component {
  public render(): React.ReactNode {
    return (
      <div>
        <Layout>
          <BurgerBuilder />
        </Layout>
      </div>
    );
  }
}

// Exports
export default App;
