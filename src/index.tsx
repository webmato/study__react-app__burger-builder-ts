// Dependencies
import * as React from 'react';
import * as ReactDOM from 'react-dom';

// Modules
import App from './App';
import registerServiceWorker from './registerServiceWorker';

// Assets
import './index.css';

// Types and Interfaces

// Component
ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();

// Exports
